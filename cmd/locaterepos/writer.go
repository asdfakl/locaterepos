package main

import (
	"bufio"
	"os"
	"sort"
	"strings"
)

type Writer interface {
	Write(string)
	Flush()
}

type LineBufferedWriter struct {
	Delimiter byte
}

func (w *LineBufferedWriter) Write(path string) {
	_, err := os.Stdout.Write(append([]byte(path), w.Delimiter))
	must(err, "line buffered writer write")
}

func (w *LineBufferedWriter) Flush() {}

type BufferedWriter struct {
	Delimiter byte
	Sort      bool
	Trim      bool

	paths  []string
	prefix []rune
}

func (w *BufferedWriter) Write(path string) {
	if w.Trim {
		if w.prefix == nil {
			w.prefix = []rune(path)
		} else {
			i := 0
			for _, r := range path {
				if i >= len(w.prefix) {
					break
				}
				if w.prefix[i] != r {
					w.prefix = w.prefix[0:i]
				}

				i++
			}
		}
	}

	w.paths = append(w.paths, path)
}

func (w *BufferedWriter) Flush() {
	if w.Sort {
		sort.Strings(w.paths)
	}

	if w.Trim {
		for i := range w.paths {
			w.paths[i] = strings.TrimPrefix(w.paths[i], string(w.prefix))
		}
	}

	bufw := bufio.NewWriter(os.Stdout)

	for _, path := range w.paths {
		_, err := bufw.Write(append([]byte(path), w.Delimiter))
		must(err, "buffered writer write")
	}

	must(bufw.Flush(), "buffered writer flush")
}
