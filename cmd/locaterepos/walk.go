package main

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

func walk(cfg *Configuration) error {
	root := &DirEntry{
		Path: cfg.RootPath,
	}

	var (
		activeCount = 0
		errc        = make(chan error)
		workc       = make(chan []*DirEntry)
		repoc       = make(chan *DirEntry)
		donec       = make(chan struct{})
		writer      Writer
	)

	if cfg.Sort || cfg.Trim {
		writer = &BufferedWriter{
			Delimiter: cfg.Delimiter(),
			Sort:      cfg.Sort,
			Trim:      cfg.Trim,
		}
	} else {
		writer = &LineBufferedWriter{Delimiter: cfg.Delimiter()}
	}

	spawn := func(dir *DirEntry) {
		activeCount++
		go work(dir, cfg, workc, repoc, errc, donec)
	}

	spawn(root)

	work := []*DirEntry{}

	for {
		select {
		case err := <-errc:
			if !cfg.IgnoreErrors {
				return err
			}
			fmt.Fprintf(os.Stderr, "warn: walk: %v\n", err)

		case dirs := <-workc:
			work = append(work, dirs...)

		case repo := <-repoc:
			writer.Write(repo.Path)

		case <-donec:
			activeCount--

			for activeCount < cfg.WorkerCount && len(work) != 0 {
				dir := work[0]
				work = work[1:]

				spawn(dir)
			}

			if activeCount == 0 && len(work) == 0 {
				writer.Flush()
				return nil
			}
		}

	}
}

func work(
	dir *DirEntry,
	cfg *Configuration,
	workc chan<- []*DirEntry,
	repoc chan<- *DirEntry,
	errc chan<- error,
	donec chan<- struct{},
) {
	defer func() {
		donec <- struct{}{}
	}()

	if dir.Level > cfg.RecursionLimit {
		fmt.Fprintf(os.Stderr, "warn: recursion limit %d exceeded\n", cfg.RecursionLimit)
		return
	}

	ls, err := os.ReadDir(dir.Path)
	if err != nil {
		errc <- fmt.Errorf("read directory: %w", err)
		return
	}

	work := []*DirEntry{}
	for _, candidate := range ls {
		if !candidate.IsDir() {
			continue
		}

		if candidate.Name() == ".git" {
			repoc <- dir
			return
		}

		if !cfg.IncludeHidden && strings.HasPrefix(candidate.Name(), ".") {
			continue
		}

		work = append(work, &DirEntry{
			Path:  filepath.Join(dir.Path, candidate.Name()),
			Level: dir.Level + 1,
		})
	}

	if len(work) != 0 {
		workc <- work
	}
}
