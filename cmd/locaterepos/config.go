package main

import (
	"errors"
	"flag"
	"fmt"
	"os"
	"runtime"
	"strings"
)

const DEFAULT_RECURSION_LIMIT = 30

func usage() {
	const usage = `
Usage: %s [OPTIONS] [ROOT]

Recursively finds git repositories located under ROOT directory.

Does not traverse into repositories to find submodules.

	`

	fmt.Fprintf(os.Stderr, strings.TrimSpace(usage)+"\n\n", os.Args[0])

	flag.PrintDefaults()

	os.Exit(1)
}

type Configuration struct {
	RootPath       string
	WorkerCount    int
	RecursionLimit int
	IncludeHidden  bool
	IgnoreErrors   bool
	Sort           bool
	Trim           bool
	NullDelimited  bool
}

func (cfg *Configuration) Delimiter() byte {
	if cfg.NullDelimited {
		return 0
	}
	return '\n'
}

func defaultConfiguration() *Configuration {
	return &Configuration{
		WorkerCount:    runtime.NumCPU(),
		RecursionLimit: DEFAULT_RECURSION_LIMIT,
	}
}

func (cfg *Configuration) validate() error {
	for _, assert := range []struct {
		ok  bool
		msg string
	}{
		{
			cfg.WorkerCount > 0,
			"worker-count must be positive",
		},
		{
			cfg.RecursionLimit > 0 && cfg.RecursionLimit <= 80,
			"recursion-limit must be in the range (0, 80]",
		},
	} {
		if !assert.ok {
			return errors.New(assert.msg)
		}
	}

	return nil
}
