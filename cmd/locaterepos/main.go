package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
)

func must(err error, ctx string) {
	if err != nil {
		die(err, ctx)
	}
}

func die(err error, ctx string) {
	fmt.Fprintf(os.Stderr, "fatal: %s: %v\n", ctx, err)
	os.Exit(2)
}

func main() {
	cfg := defaultConfiguration()

	flag.IntVar(&cfg.WorkerCount, "worker-count", cfg.WorkerCount, "number of paraller workers")
	flag.IntVar(&cfg.RecursionLimit, "recursion-limit", cfg.RecursionLimit, "maximum recursion depth")
	flag.BoolVar(&cfg.IncludeHidden, "include-hidden", cfg.IncludeHidden, "recurse into hidden directories")
	flag.BoolVar(&cfg.IgnoreErrors, "ignore-errors", cfg.IgnoreErrors, "continue despite non-fatal errors")
	flag.BoolVar(&cfg.Sort, "sort", cfg.Sort, "sorted output")
	flag.BoolVar(&cfg.Trim, "trim", cfg.Trim, "trim common prefix")
	flag.BoolVar(&cfg.NullDelimited, "null-delimited", cfg.NullDelimited, "use '\\0' as output delimiter instead of '\\n'")

	flag.Usage = usage
	flag.Parse()
	args := flag.Args()

	if len(args) != 1 {
		flag.Usage()
	}

	root, err := filepath.EvalSymlinks(args[0])
	must(err, "eval root symlinks")

	root, err = filepath.Abs(root)
	must(err, "determine absolute path to root")

	cfg.RootPath = root
	must(cfg.validate(), "validate configuration")

	must(walk(cfg), "walk")
}
